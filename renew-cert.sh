#!/bin/sh

yarn global add gitlab-letsencrypt

gitlab-le \
  --email clayton@hynfield.org \
  --domain clayton.hynfield.org \
  --repository https://gitlab.com/clhynfield/clhynfield.gitlab.io \
  --token "$TLS_RENEWAL_TOKEN" \
  --path app/.well-known/acme-challenge \
  --production
