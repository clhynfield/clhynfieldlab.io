# Three Qualities of Thinking

## Design thinking

- Deeply heartfelt empathy for the user
- Keen appreciation that good [form ever follows function][sull1896]
- Two decades study of [digital][ala] [masters][coudal]

[sull1896]: https://archive.org/details/tallofficebuildi00sull
[ala]: https://alistapart.com/authors
[coudal]: http://www.coudal.com/about.php

## Engineering thinking

- Antifragility, chaos engineering
- TDD + declarative, immutable infrastructure e.g. [BOSH][bosh]

[bosh]: https://bosh.io/

## Business thinking

- Build vs buy SaaS, PaaS, IaaS is about "value line"
