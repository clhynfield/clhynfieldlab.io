import gulp from 'gulp';
import del from 'del';
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;
const $ = require('gulp-load-plugins')();

var dev = false;

gulp.task('styles', () => {
  return gulp.src('app/styles/**/*.@(css|less)')
    .pipe($.if(dev === false && /\.less$/, $.less()))
    .pipe($.if(dev, $.sourcemaps.init()))
    .pipe($.if(dev === false, $.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']})))
    .pipe($.if(dev, $.sourcemaps.write()))
    .pipe(gulp.dest('.tmp/styles'))
    .pipe(reload({stream: true}));
});

gulp.task('scripts', () => {
  return gulp.src('app/scripts/**/*.js')
    .pipe($.plumber())
    .pipe($.if(dev, $.sourcemaps.init()))
    .pipe($.babel())
    .pipe($.if(dev, $.sourcemaps.write('.')))
    .pipe(gulp.dest('.tmp/scripts'))
    .pipe(reload({stream: true}));
});

function lint(files) {
  return gulp.src(files)
    .pipe($.eslint({ fix: true }))
    .pipe(reload({stream: true, once: true}))
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
}

gulp.task('lint', () => {
  return lint('app/scripts/**/*.js')
    .pipe(gulp.dest('app/scripts'));
});
gulp.task('lint:test', () => {
  return lint('test/spec/**/*.js')
    .pipe(gulp.dest('test/spec'));
});

gulp.task('html', gulp.series(gulp.parallel('styles', 'scripts'), () => {
  return gulp.src(['app/**/*.html', 'app/.*/**'])
    .pipe($.regexReplace({regex:'\.less\"', replace:'.css"'}))
    .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
    .pipe($.if(/\.js$/, $.uglify({compress: {drop_console: true}})))
    .pipe($.if(/\.css$/, $.cssnano({safe: true, autoprefixer: false})))
    .pipe($.if(/\.html$/, $.htmlmin({
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: {compress: {drop_console: true}},
      processConditionalComments: true,
      removeComments: true,
      removeEmptyAttributes: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true
    })))
    .pipe(gulp.dest('public'));
}));

gulp.task('images', () => {
  return gulp.src('app/images/**/*')
    .pipe($.cache($.imagemin()))
    .pipe(gulp.dest('public/images'));
});

gulp.task('fonts', () => {
  return gulp.src('app/fonts/**/*')
    .pipe($.if(dev, gulp.dest('.tmp/fonts'), gulp.dest('public/fonts')));
});

gulp.task('extras', () => {
  return gulp.src([
    'app/*',
    '!app/*.html'
  ], {
    dot: true
  }).pipe(gulp.dest('public'));
});

gulp.task('clean', del.bind(null, ['.tmp', 'public']));

gulp.task('serve', gulp.series('clean', 'scripts', 'styles', 'fonts', () => {
  browserSync.init({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['.tmp', 'app'],
      routes: {
      }
    }
  });

  gulp.watch([
    'app/*.html',
    'app/images/**/*',
    '.tmp/fonts/**/*'
  ]).on('change', reload);

  gulp.watch('app/styles/**/*.@(css|less)', gulp.series('styles'));
  gulp.watch('app/scripts/**/*.js', gulp.series('scripts'));
  gulp.watch('app/fonts/**/*', gulp.series('fonts'));
}));

gulp.task('serve:test', gulp.series('scripts', () => {
  browserSync.init({
    notify: false,
    port: 9000,
    ui: false,
    server: {
      baseDir: 'test',
      routes: {
        '/scripts': '.tmp/scripts',
        '/bower_components': 'bower_components'
      }
    }
  });

  gulp.watch('app/scripts/**/*.js', gulp.series('scripts'));
  gulp.watch(['test/spec/**/*.js', 'test/index.html']).on('change', reload);
  gulp.watch('test/spec/**/*.js', gulp.series('lint:test'));
}));

gulp.task('build', gulp.series(gulp.parallel('lint', 'html', 'images', 'fonts', 'extras'), () => {
  return gulp.src('public/**/*').pipe($.size({title: 'build', gzip: true}));
}));

gulp.task('default', gulp.series('clean', 'scripts', 'build'));

gulp.task('serve:public', gulp.series('default', () => {
  browserSync.init({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['public']
    }
  });
}));
